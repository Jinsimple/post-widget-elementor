<?php 
$args = array( 
	'post_type'   => 'post',
);
$post = new WP_Query( $args );

if ( $post->have_posts() ) : 
?>
<div class="widget-post grid-style">
	<?php while( $post->have_posts() ) : $post->the_post() ?>
		<div class="widget-post__item">
			<div class="widget-post__img"><?php the_post_thumbnail(); ?></div>
			<p class="widget-post__title"><?php the_title(); ?></p>
			<span><?php the_excerpt(); ?></span>
		</div>
	<?php endwhile ?>
	<?php else : ?>
		<!-- Content If No Posts -->
	<?php endif ?>
</div>
<?php 
/* Restore original Post Data */
wp_reset_postdata(); ?>