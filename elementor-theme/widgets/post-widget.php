<?php
namespace ElementorTheme\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Post_Widget extends Widget_Base {

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'post-widget-custom';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Post widget', 'SevenUpWidget-widget' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-atom';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends() {
		return [ 'SevenUpWidget-widget' ];
    }
    
    public function get_style_depends() {
		return array( 'post_custom_style' );
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'SevenUpWidget-widget' ),
			]
        );
        
        $this->add_control(
			'style_transform',
			[
				'label' => __( 'Style show widget', 'SevenUpWidget-widget' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'List', 'SevenUpWidget-widget' ),
					'grid' => __( 'Grid', 'SevenUpWidget-widget' ),
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'style_transform: {{VALUE}};',
				],
            ]
        );

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'SevenUpWidget-widget' ),
				'type' => Controls_Manager::TEXT,
			]
        );
        $this->add_control(
			'description',
			[
				'label' => __( 'Description', 'SevenUpWidget-widget' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __( 'Description', 'SevenUpWidget-widget' ),
			]
		);

		$this->add_control(
			'content',
			[
				'label' => __( 'Content', 'SevenUpWidget-widget' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( 'Content', 'SevenUpWidget-widget' ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Style', 'SevenUpWidget-widget' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
            'text_transform',
			[
				'label' => __( 'Text Transform', 'SevenUpWidget-widget' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'None', 'SevenUpWidget-widget' ),
					'uppercase' => __( 'UPPERCASE', 'SevenUpWidget-widget' ),
					'lowercase' => __( 'lowercase', 'SevenUpWidget-widget' ),
					'capitalize' => __( 'Capitalize', 'SevenUpWidget-widget' ),
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'text-transform: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_inline_editing_attributes( 'title', 'none' );
		$this->add_inline_editing_attributes( 'description', 'basic' );
		$this->add_inline_editing_attributes( 'content', 'advanced' );
        ?>
        <div class="post-widget">
            <?php if ($settings['title'] != ''): ?>
                <h2 <?php echo $this->get_render_attribute_string( 'title' ); ?>><?php echo $settings['title']; ?></h2>
            <?php endif ?>

            <?php if ($settings['description'] != ''): ?>
                <div <?php echo $this->get_render_attribute_string( 'description' ); ?>><?php echo $settings['description']; ?></div>
            <?php endif ?>

            <?php if ($settings['content'] != ''): ?>
                <div <?php echo $this->get_render_attribute_string( 'content' ); ?>><?php echo $settings['content']; ?></div>
            <?php endif ?>
            <?php
            // Choose style widget to show
            $option = 'list';
            if($settings['style_transform'] !== ''){
                $option = $settings['style_transform'];
                require_once( __DIR__ . '/../template/post/'.$option.'.php' );
            }
            require_once( __DIR__ . '/../template/post/'.$option.'.php' );
            ?>
        </div>
        <?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<#
		view.addInlineEditingAttributes( 'title', 'none' );
		view.addInlineEditingAttributes( 'description', 'basic' );
		view.addInlineEditingAttributes( 'content', 'advanced' );
		#>
		<h2 {{{ view.getRenderAttributeString( 'title' ) }}}>{{{ settings.title }}}</h2>
		<div {{{ view.getRenderAttributeString( 'description' ) }}}>{{{ settings.description }}}</div>
		<div {{{ view.getRenderAttributeString( 'content' ) }}}>{{{ settings.content }}}</div>
		<?php
	}
}
